# This is provided solely for purposes of `make install'

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(SHAREDIR)/man

all:

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f latexmk-quieter $(DESTDIR)$(BINDIR)/latexmk-quieter

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f latexmk-quieter
